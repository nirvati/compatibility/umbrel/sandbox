use crate::api::{
    InstallAppRequest, InstallAppResponse, LoadAppComposeRequest, LoadAppComposeResponse,
    StartAppRequest, StartAppResponse, StopAppRequest, StopAppResponse, UninstallAppRequest,
    UninstallAppResponse,
};
use tonic::{Request, Response, Status};

mod api {
    tonic::include_proto!("nirvati_umbrel_runtime");
}

struct ApiServer;

#[tonic::async_trait]
impl api::umbrel_runtime_server::UmbrelRuntime for ApiServer {
    async fn start_app(
        &self,
        request: Request<StartAppRequest>,
    ) -> Result<Response<StartAppResponse>, Status> {
        let request = request.into_inner();
        let app_id = request.app_id;
        let cmd = tokio::process::Command::new("/fake-root/scripts/app")
            .arg("start")
            .arg(app_id)
            .output()
            .await
            .map_err(|e| Status::internal(format!("failed to execute process: {}", e)))?;
        let response = StartAppResponse { success: cmd.status.success() };
        Ok(Response::new(response))
    }

    async fn stop_app(
        &self,
        request: Request<StopAppRequest>,
    ) -> Result<Response<StopAppResponse>, Status> {
        let request = request.into_inner();
        let app_id = request.app_id;
        let cmd = tokio::process::Command::new("/fake-root/scripts/app")
            .arg("stop")
            .arg(app_id)
            .output()
            .await
            .map_err(|e| Status::internal(format!("failed to execute process: {}", e)))?;
        let response = StopAppResponse {
            success: cmd.status.success(),
        };
        Ok(Response::new(response))
    }

    async fn install_app(
        &self,
        request: Request<InstallAppRequest>,
    ) -> Result<Response<InstallAppResponse>, Status> {
        let request = request.into_inner();
        let app_id = request.app_id;
        let cmd = tokio::process::Command::new("/fake-root/scripts/app")
            .arg("install")
            .arg(app_id)
            .output()
            .await
            .map_err(|e| Status::internal(format!("failed to execute process: {}", e)))?;
        let response = InstallAppResponse {
            success: cmd.status.success(),
        };
        Ok(Response::new(response))
    }

    async fn uninstall_app(
        &self,
        request: Request<UninstallAppRequest>,
    ) -> Result<Response<UninstallAppResponse>, Status> {
        let request = request.into_inner();
        let app_id = request.app_id;
        let cmd = tokio::process::Command::new("/fake-root/scripts/app")
            .arg("uninstall")
            .arg(app_id)
            .output()
            .await
            .map_err(|e| Status::internal(format!("failed to execute process: {}", e)))?;
        let response = UninstallAppResponse {
            success: cmd.status.success(),
        };
        Ok(Response::new(response))
    }

    async fn load_app_compose(
        &self,
        request: Request<LoadAppComposeRequest>,
    ) -> Result<Response<LoadAppComposeResponse>, Status> {
        let request = request.into_inner();
        let app_id = request.app_id;
        let cmd = tokio::process::Command::new("/fake-root/scripts/app")
            .arg("compose")
            .arg(app_id)
            .arg("print-file")
            .output()
            .await
            .map_err(|e| Status::internal(format!("failed to execute process: {}", e)))?;
        let output = String::from_utf8_lossy(&cmd.stdout);
        let response = LoadAppComposeResponse {
            compose: output.to_string(),
        };
        Ok(Response::new(response))
    }
}

#[tokio::main]
async fn main() {
    let addr = "[::]:50051".parse().unwrap();
    let server = ApiServer {};
    tonic::transport::Server::builder()
        .add_service(api::umbrel_runtime_server::UmbrelRuntimeServer::new(server))
        .serve(addr)
        .await
        .unwrap();
}
